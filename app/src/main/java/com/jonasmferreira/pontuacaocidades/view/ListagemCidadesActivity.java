package com.jonasmferreira.pontuacaocidades.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.jonasmferreira.pontuacaocidades.R;
import com.jonasmferreira.pontuacaocidades.adapter.CidadeAdapter;
import com.jonasmferreira.pontuacaocidades.domain.Cidade;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListagemCidadesActivity extends BaseActivity {
    private static String TAG = ListagemCidadesActivity.class.getSimpleName();
    List<Cidade> cidades = new ArrayList<>();
    private RecyclerView rvResultado;
    private BroadcastReceiver broadReceiver;
    private ListagemCidadesActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem_cidades);
        activity = this;
        setTitle("Resultado");
        initView();
        loadData();
    }

    private void initView() {
        rvResultado = (RecyclerView) findViewById(R.id.rvResultado);
        rvResultado.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvResultado.setLayoutManager(layoutManager);
    }

    private void loadData(){
        cidades = new ArrayList<>();
        try{
            JSONArray arrCidades = new JSONArray(getIntent().getStringExtra("cidades"));
            for(int i=0;i<arrCidades.length();i++){
                JSONObject cid = arrCidades.getJSONObject(i);
                cidades.add(new Cidade(cid.getString("Nome"),cid.getString("Estado"),cid.toString()));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        CidadeAdapter adapter = new CidadeAdapter(getApplicationContext(),cidades,activity);
        rvResultado.setAdapter(adapter);
    }

    @Override
    protected void onResume(){
        super.onResume();
        loadBroadcasts();
    }

    @Override
    protected void onPause (){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadReceiver);
        closeProgressDialog();
    }

    private void loadBroadcasts(){
        broadReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive (Context context, Intent intent){
                String action = intent.getAction();
                Log.d(TAG, "onReceive: Action->"+action);
                closeProgressDialog();
                switch (action)
                {
                    case "BuscaPontos":
                    {
                        if (intent.getExtras().getBoolean("Success", false)){
                            //alert("Sucesso",intent.getStringExtra("Message"));
                            Intent it = new Intent(context,PontoCidadeActivity.class);
                            it.putExtra("cidade",intent.getStringExtra("Cidade"));
                            it.putExtra("pontos",intent.getStringExtra("Pontos"));
                            context.startActivity(it);
                        }else{
                            Log.e(TAG, "onReceive: Falha na sincronicação");
                            alert("Erro",intent.getStringExtra("Message"));
                        }
                        break;
                    }
                    default:{

                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("BuscaPontos");
        LocalBroadcastManager.getInstance(this).registerReceiver(broadReceiver, filter);
    }
}
