package com.jonasmferreira.pontuacaocidades.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.widget.TextView;

import com.jonasmferreira.pontuacaocidades.util.Helper;
import com.jonasmferreira.pontuacaocidades.util.MyProgressDialog;

public class BaseActivity extends AppCompatActivity {
    protected MyProgressDialog progressDialog;
    protected Context context;
    Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        activity = this;
        progressDialog = new MyProgressDialog(context);
    }
    public void showProgressDialog(){
        progressDialog.show();
    }
    protected void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    protected void alert(String title, String msg) {

        TextView tv1 = new TextView(context);
        tv1.setPadding(30, 30, 30, 30);
        tv1.setText(title);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv1.setTextColor(Color.BLACK);


        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCustomTitle(tv1);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView tv2 = (TextView) alertDialog.findViewById(android.R.id.message);
    }

    protected void alert(String title, String msg, final Class<?> classe) {

        TextView tv1 = new TextView(context);
        tv1.setPadding(30, 30, 30, 30);
        tv1.setText(title);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv1.setTextColor(Color.BLACK);


        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCustomTitle(tv1);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Helper.goTo(context, classe);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView tv2 = (TextView) alertDialog.findViewById(android.R.id.message);
    }

    private void askForPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            }
        } else {
            execPermissionGranted(requestCode);
        }
    }

    protected void execPermissionGranted(int requestCode){

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED){
            execPermissionGranted(requestCode);
        }
    }
}
