package com.jonasmferreira.pontuacaocidades.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.jonasmferreira.pontuacaocidades.R;

public class PontoCidadeActivity extends AppCompatActivity {

    private TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ponto_cidade);
        setTitle("Resultado");
        initView();
        loadData();
    }

    private void initView() {
        tvResultado = (TextView) findViewById(R.id.tvResultado);
    }

    private void loadData(){
        String res = "A pontuação da cidade "+getIntent().getStringExtra("cidade")+" é "+getIntent().getStringExtra("pontos");
        tvResultado.setText(res);
    }
}
