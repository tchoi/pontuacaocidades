package com.jonasmferreira.pontuacaocidades.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.jonasmferreira.pontuacaocidades.R;
import com.jonasmferreira.pontuacaocidades.controller.CidadeController;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    protected static String TAG = MainActivity.class.getSimpleName();
    private EditText etCidade;
    private EditText etEstado;
    private Button btBuscar;
    private BroadcastReceiver broadReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        setTitle("Busca");
    }

    private void initView() {
        etCidade = (EditText) findViewById(R.id.etCidade);
        etEstado = (EditText) findViewById(R.id.etEstado);
        btBuscar = (Button) findViewById(R.id.btBuscar);

        btBuscar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btBuscar:
                submit();
                break;
        }
    }
    @Override
    protected void onResume(){
        super.onResume();
        loadBroadcasts();
    }

    @Override
    protected void onPause (){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadReceiver);
        closeProgressDialog();
    }

    private void loadBroadcasts(){
        broadReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive (Context context, Intent intent){
                String action = intent.getAction();
                Log.d(TAG, "onReceive: Action->"+action);
                closeProgressDialog();
                switch (action)
                {
                    case "BuscaTodasCidades":
                    {
                        if (intent.getExtras().getBoolean("Success", false)){
                            //alert("Sucesso",intent.getStringExtra("Message"));
                            Intent it = new Intent(context,ListagemCidadesActivity.class);
                            it.putExtra("cidades",intent.getStringExtra("Data"));
                            context.startActivity(it);
                        }else{
                            Log.e(TAG, "onReceive: Falha na sincronicação");
                            alert("Erro",intent.getStringExtra("Message"));
                        }
                        break;
                    }
                    default:{

                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("BuscaTodasCidades");
        LocalBroadcastManager.getInstance(this).registerReceiver(broadReceiver, filter);
    }

    private void submit() {
        showProgressDialog();
        CidadeController.BuscaTodasCidades(context,etCidade.getText().toString(),etEstado.getText().toString());
    }
}
