package com.jonasmferreira.pontuacaocidades.controller;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.jonasmferreira.pontuacaocidades.R;
import com.jonasmferreira.pontuacaocidades.util.App;
import com.jonasmferreira.pontuacaocidades.util.Helper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class CidadeController {
    protected static String url = App.getInstance().getString(R.string.SERVER_BASE_URL);
    protected static String TAG = CidadeController.class.getSimpleName();
    public static void BuscaTodasCidades(final Context context, final String cidade, final String estado){
        final String endpoint = url+"BuscaTodasCidades";
        final String broadcastAction = "BuscaTodasCidades";
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60*1000);
        client.get(endpoint,new JsonHttpResponseHandler()
        {

            @Override
            public void onStart (){
            }

            @Override
            public void onSuccess (int statusCode, Header[] headers, JSONArray response){
                Log.d(TAG, "onSuccess: response->"+response.toString());
                Bundle broadBundle = new Bundle();

                try{
                    broadBundle.putBoolean("Success", true);
                    broadBundle.putString("Message", "Cidades buscadas com sucesso");
                    //CidadeModel.delete();
                    JSONArray locate = new JSONArray();
                    for(int i=0;i<response.length();i++){
                        JSONObject o = response.getJSONObject(i);
                        if(Helper.containsIgnoreCase(o.getString("Nome"),cidade) && Helper.containsIgnoreCase(o.getString("Estado"),estado)){
                            //CidadeModel.insertCidade(response.getJSONObject(i));
                            locate.put(o);
                        }
                    }
                    broadBundle.putString("Data", locate.toString());
                }catch (Exception ex){
                    ex.printStackTrace();
                    broadBundle.putBoolean("Success", false);
                    Log.e(TAG, "onSuccess: Exception->"+ex.getLocalizedMessage());
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse){
                String replyMsg = "Falha " + statusCode + ": " + throwable.getLocalizedMessage();
                Log.e(TAG, "onFailure: "+replyMsg+"->"+(errorResponse != null ? errorResponse.toString() : "nl | " +  throwable.getMessage()));
                Bundle broadBundle = new Bundle();

                String msg = "Falha de conexão com o Servidor [" + statusCode + "]";
                try {
                    msg = errorResponse.has("msg") ? errorResponse.getString("msg") : replyMsg;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                broadBundle.putString("Message", msg);
                broadBundle.putBoolean("Success", false);


                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse){
                Bundle broadBundle = new Bundle();
                broadBundle.putBoolean("Success", false);
                broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, String responseString, Throwable throwable){
                Bundle broadBundle = new Bundle();

                broadBundle.putBoolean("Success", false);
                broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }
            @Override
            public void onFinish(){

            }
        });
    }

    public static void BuscaPontos(final Context context, final String cidade, final String json){
        final String endpoint = url+"BuscaPontos";
        final String broadcastAction = "BuscaPontos";
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60*1000);

        StringEntity entity = null;
        try {
            entity = new StringEntity(json);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        client.post(context,endpoint, entity,"application/json",new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String response) {
                // called when response HTTP status is "200 OK"
                Log.d(TAG, "onSuccess: response->"+response.toString());
                Bundle broadBundle = new Bundle();
                broadBundle.putBoolean("Success", true);
                broadBundle.putString("Message", "Cidades buscadas com sucesso");
                broadBundle.putString("Cidade", cidade);
                broadBundle.putString("Pontos", response);
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String v, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Bundle broadBundle = new Bundle();

                broadBundle.putBoolean("Success", false);
                broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }
        });
    }
}
