package com.jonasmferreira.pontuacaocidades.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jonasmferreira.pontuacaocidades.R;
import com.jonasmferreira.pontuacaocidades.controller.CidadeController;
import com.jonasmferreira.pontuacaocidades.domain.Cidade;
import com.jonasmferreira.pontuacaocidades.view.ListagemCidadesActivity;

import java.util.List;

public class CidadeAdapter extends RecyclerView.Adapter<CidadeAdapter.ViewHolder>{
    protected static String TAG = CidadeAdapter.class.getSimpleName();
    private Context context;
    private List<Cidade> cidades;
    private ListagemCidadesActivity activity;

    public CidadeAdapter(Context context, List<Cidade> cidades) {
        this.context = context;
        this.cidades = cidades;
    }


    public CidadeAdapter(Context context, List<Cidade> cidades, ListagemCidadesActivity activity) {
        this.context = context;
        this.cidades = cidades;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cidade, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final int i = position;
        holder.tvCidade.setText(cidades.get(i).nome);
        holder.tvEstado.setText(cidades.get(i).estado);
        holder.llItemCidade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: "+cidades.get(i).toString());
                activity.showProgressDialog();
                CidadeController.BuscaPontos(context,cidades.get(i).nome,cidades.get(i).json);

            }
        });

    }

    @Override
    public int getItemCount() {
        return cidades.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvCidade;
        private TextView tvEstado;
        private LinearLayout llItemCidade;
        public ViewHolder(View view) {
            super(view);
            tvCidade = (TextView)view.findViewById(R.id.tvCidade);
            tvEstado = (TextView)view.findViewById(R.id.tvEstado);
            llItemCidade = (LinearLayout)view.findViewById(R.id.llItemCidade);

        }
    }
}
