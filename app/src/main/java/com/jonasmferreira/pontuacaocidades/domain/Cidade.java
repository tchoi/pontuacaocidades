package com.jonasmferreira.pontuacaocidades.domain;

public class Cidade{
    public String nome;
    public String estado;
    public String json;

    public Cidade() {
    }

    public Cidade(String nome, String estado, String json) {
        this.nome = nome;
        this.estado = estado;
        this.json = json;
    }

    @Override
    public String toString() {
        return "Cidade{" +
                "nome='" + nome + '\'' +
                ", estado='" + estado + '\'' +
                ", json='" + json + '\'' +
                '}';
    }
}
